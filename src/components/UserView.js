import { Fragment, useState, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import { Helmet } from "react-helmet-async";
import ProductCard from "./ProductCard";

export default function UserView({ productData }) {
  console.log(productData);

  const [product, setProduct] = useState([]);

  useEffect(() => {
    const productArr = productData.map((product) => {
      if (product.isActive === true) {
        return (
          <Col key={product.slug} sm={6} md={4} lg={3} className="product">
            <ProductCard productProp={product} key={product._id} />
          </Col>
        );
      } else {
        return null;
      }
    });

    setProduct(productArr);
  }, [productData]);

  return (
    <Fragment>
      <Helmet>
        <title>Products</title>
      </Helmet>
      <main className="pagePadding">
        <h1>Featured Products</h1>
        <Row className="products">{product}</Row>
      </main>
    </Fragment>
  );
}
