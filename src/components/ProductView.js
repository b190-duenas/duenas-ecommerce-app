import { useState, useEffect, useContext } from "react";
import {
  Container,
  Card,
  Button,
  Row,
  Col,
  ListGroup,
  Badge,
} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import Rating from "./RatingStar";
import { Store } from "../Store";

export default function ProductView() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams();
  const [product, setProduct] = useState({});
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);
  const [rating, setRating] = useState(0);
  const [numReviews, setNumReviews] = useState(0);

  useEffect(() => {
    console.log(productId);

    fetch(`https://fish-be-with-you.herokuapp.com/product/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setImage(data.image);
        setDescription(data.description);
        setPrice(data.price);
        setStocks(data.stocks);
        setRating(data.rating);
        setNumReviews(data.numReviews);
        setProduct(data);
      });
  }, [productId]);

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart } = state;
  const addToCartHandler = () => {
    const existItem = cart.cartItems.find((x) => x._id === productId);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const productData = fetch(
      `https://fish-be-with-you.herokuapp.com/product/${productId}`
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.stocks < quantity) {
          window.alert("Product is out of stock");
          return;
        }
      });
    ctxDispatch({
      type: "CART_ADD_ITEM",
      payload: { ...product, quantity },
    });
    navigate("/cart");
  };

  return (
    <Container className="pagePadding">
      <Row>
        <Col md={6}>
          <img className="img-product-view" src={image} alt={name}></img>
        </Col>
        <Col md={3}>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <Helmet>
                <title>{name}</title>
              </Helmet>
              <h1>{name}</h1>
            </ListGroup.Item>
            <ListGroup.Item>
              <Rating rating={rating} numReviews={numReviews}></Rating>
            </ListGroup.Item>
            <ListGroup.Item>Price : ${price}</ListGroup.Item>
            <ListGroup.Item>Description : {description}</ListGroup.Item>
            <ListGroup.Item></ListGroup.Item>
          </ListGroup>
        </Col>
        <Col md={3}>
          <Card>
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <Row>
                    <Col>Price:</Col>
                    <Col>${price}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Status:</Col>
                    <Col>
                      {stocks > 0 ? (
                        <Badge bg="success">In Stock</Badge>
                      ) : (
                        <Badge bg="danger">Out of Stock</Badge>
                      )}
                    </Col>
                  </Row>
                </ListGroup.Item>
                {stocks > 0 && (
                  <ListGroup.Item>
                    <div className="d-grid">
                      <Button onClick={addToCartHandler} variant="primary">
                        Add to Cart
                      </Button>
                    </div>
                  </ListGroup.Item>
                )}
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
