import { useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Store } from "../Store";
import Rating from "./RatingStar";

export default function ProductCard({ productProp }) {
  const { name, image, description, price, stocks, _id, rating, numReviews } =
    productProp;

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    cart: { cartItems },
  } = state;

  const addToCartHandler = async (item) => {
    const existItem = cartItems.find((x) => x._id === item._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const productData = fetch(
      `https://fish-be-with-you.herokuapp.com/product/${productProp._id}`
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.stocks < quantity) {
          window.alert("Product is out of stock");
          return;
        }
      });
    ctxDispatch({
      type: "CART_ADD_ITEM",
      payload: { ...item, quantity },
    });
  };

  return (
    <Card className="card align-items-center">
      <Card.Body>
        <img src={image} alt={name} className="img-product"></img>
        <Row>
          <Col className="text-center">
            <Card.Title>
              <Link to={`/product/${_id}`}>{name}</Link>
            </Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>₱{price}</Card.Text>

            <Card.Subtitle>Stocks:</Card.Subtitle>
            <Card.Text>{stocks}</Card.Text>
          </Col>
          <Col className="text-center">
            <Rating rating={rating} numReviews={numReviews}></Rating>
          </Col>
        </Row>
        <Row className="mx-auto mt-2">
          {productProp.stocks === 0 ? (
            <Button variant="muted" disabled={true}>
              Out of Stock
            </Button>
          ) : (
            <Button onClick={() => addToCartHandler(productProp)}>
              Add to Cart
            </Button>
          )}
        </Row>
      </Card.Body>
    </Card>
  );
}
