// react permits the use of object literals if the dev will import multiple components from the same dependency
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../images/FBWYLogo.png";
export default function Banner({ data }) {
  const { title, content, destination, button } = data;
  return (
    <div className="homeBanner pagePadding">
      <Row className="h-100">
        <Col></Col>
        <Col md={6} className="homeRight">
          <img src={logo} alt="Fish be with you" className="logo"></img>
          <h3>{content}</h3>
          <Link to={destination}>
            <Button className="mt-3 text-center">{button}</Button>
          </Link>
        </Col>
      </Row>
    </div>
  );
}
