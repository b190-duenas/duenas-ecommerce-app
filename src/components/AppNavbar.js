import { useContext } from "react";

import { Fragment } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import { Badge, NavDropdown } from "react-bootstrap";
import { Store } from "../Store";
import logo from "../images/FBWYLogo2.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";

export default function AppNavbar() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart } = state;
  const { user } = useContext(UserContext);
  const name = localStorage.getItem("name");
  console.log(user);

  const signoutHandler = () => {
    ctxDispatch({ type: "USER_SIGNOUT" });
    localStorage.removeItem("cartItems");
    localStorage.removeItem("shippingAddress");
    localStorage.removeItem("paymentMethod");
  };

  return (
    <Navbar className="navBar" expand="lg">
      <Navbar.Toggle aria-controls="basic-navbar-nav" />

      <Nav className="right">
        <Nav.Link as={NavLink} to="/" exact className="mini-logo">
          <img src={logo} height={40} width={40} alt="Fish be with you"></img>
        </Nav.Link>
        <Nav.Link as={NavLink} to="/" exact className="px-4">
          {" "}
          Home
        </Nav.Link>
        <Nav.Link as={NavLink} to="/products" exact className="px-4">
          {" "}
          Products
        </Nav.Link>
      </Nav>
      <Nav className="left">
        <Link to="/cart" className="nav-link">
          <FontAwesomeIcon icon={faCartShopping} />
          {cart.cartItems.length > 0 && (
            <Badge pill bg="danger">
              {cart.cartItems.reduce((acc, cur) => acc + cur.quantity, 0)}
            </Badge>
          )}
        </Link>

        {user.id !== null ? (
          <NavDropdown title={name} id="basic-nav-dropdown container">
            <Link to="/profile">
              <NavDropdown.Item>User Profile</NavDropdown.Item>
            </Link>
            <Link to="/orderHistory">
              <NavDropdown.Item>Order History</NavDropdown.Item>
            </Link>
            <NavDropdown.Divider />
            <Nav.Link as={NavLink} to="/logout" onClick={signoutHandler} exact>
              {" "}
              Logout
            </Nav.Link>
          </NavDropdown>
        ) : (
          <Fragment>
            <Nav.Link as={NavLink} to="/login" exact>
              {" "}
              Login
            </Nav.Link>
            <Nav.Link as={NavLink} to="/register" exact>
              {" "}
              Register
            </Nav.Link>
          </Fragment>
        )}
      </Nav>
    </Navbar>
  );
}
