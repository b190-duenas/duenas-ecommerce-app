import { Fragment } from "react";
import { Helmet } from "react-helmet-async";
import Banner from "../components/Banner";

export default function Home() {
  const homeData = {
    title: "Fish Be With You",
    content: "That fish is so classy, it’s like he’s so-fish-ticated",
    destination: "/products",
    button: "Build a fish empire",
  };

  return (
    <Fragment>
      <Helmet>
        <title>Fish Be With You</title>
      </Helmet>
      <Banner data={homeData} />
    </Fragment>
  );
}
