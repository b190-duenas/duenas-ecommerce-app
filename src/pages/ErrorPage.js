import { Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

export default function Error() {
  const navigate = useNavigate();

  return (
    <div className="pagePadding mx-auto container">
      <h1>Error: 404, Page Not Found</h1>
      <Link to={"/"}>
        <Button className="mt-3 text-center">Go Back To Home Page</Button>
      </Link>
    </div>
  );
}
