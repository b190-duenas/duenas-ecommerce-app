import React, { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Helmet } from "react-helmet-async";
import { useNavigate } from "react-router-dom";
import CheckoutSteps from "../components/checkoutSteps";
import { Store } from "../Store";
import UserContext from "../UserContext";

export default function ShippingAddress() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    cart: { shippingAddress },
  } = state;
  const [fullName, setFullName] = useState(shippingAddress.fullName || "");
  const [street, setStreet] = useState(shippingAddress.street || "");
  const [city, setCity] = useState(shippingAddress.city || "");
  const [province, setProvince] = useState(shippingAddress.province || "");
  const [postalCode, setPostalCode] = useState(
    shippingAddress.postalCode || ""
  );
  const [country, setCountry] = useState(shippingAddress.country || "");

  useEffect(() => {
    if (user.id === null) {
      navigate("/login?redirect=/shipping");
    }
  }, [user, navigate]);

  const submitHandler = (e) => {
    e.preventDefault();
    ctxDispatch({
      type: "ADD_ADDRESS",
      payload: {
        fullName,
        street,
        city,
        postalCode,
        province,
        country,
      },
    });
    localStorage.setItem(
      "shippingAddress",
      JSON.stringify({ fullName, street, city, postalCode, province, country })
    );
    navigate("/payment");
  };

  return (
    <div className="pagePadding container-fluid w-75 mx-auto">
      <Helmet>
        <title>Shipping Address</title>
      </Helmet>
      <CheckoutSteps step1 step2></CheckoutSteps>
      <div className="container small-container">
        <h1 className="mb-3">Shipping Address</h1>
        <Form onSubmit={submitHandler}>
          <Form.Group>
            <Form.Label>Full Name</Form.Label>
            <Form.Control
              value={fullName}
              onChange={(e) => setFullName(e.target.value)}
              required
            />
            <Form.Label>Address</Form.Label>
            <Form.Control
              value={street}
              onChange={(e) => setStreet(e.target.value)}
              required
            />
            <Form.Label>City</Form.Label>
            <Form.Control
              value={city}
              onChange={(e) => setCity(e.target.value)}
              required
            />
            <Form.Label>Postal Code</Form.Label>
            <Form.Control
              value={postalCode}
              onChange={(e) => setPostalCode(e.target.value)}
              required
            />
            <Form.Label>Province</Form.Label>
            <Form.Control
              value={province}
              onChange={(e) => setProvince(e.target.value)}
              required
            />
            <Form.Label>Country</Form.Label>
            <Form.Control
              value={country}
              onChange={(e) => setCountry(e.target.value)}
              required
            />
          </Form.Group>
          <div className="my-3">
            <Button variant="primary" type="submit">
              Continue
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
