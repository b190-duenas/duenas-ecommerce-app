import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Swal from "sweetalert2";

import UserContext from "../UserContext";
import { Helmet } from "react-helmet-async";

export default function Register() {
  /*
		Miniactivity 
			1 create email, password1 and password2 variables using useState and set their inital value to empty string ""
			2 create isActive variable using useState and set the initial value to false
			3 refactor the button and add a ternary operator stating that if the isActive is true, the button will be clickable, otherwise, disabled

			6 minutes: 6:30 pm
	*/
  // state hooks to store the values of the input fields
  const { user, setUser } = useContext(UserContext);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobile] = useState("");
  const [password, setPassword] = useState("");
  const [verifyPassword, SetVerifyPassword] = useState("");
  const navigate = useNavigate();

  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password);
  console.log(verifyPassword);

  useEffect(() => {
    if (
      email !== "" &&
      password !== "" &&
      verifyPassword !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      password === verifyPassword &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, verifyPassword, firstName, lastName, mobileNo]);

  function registerUser(e) {
    e.preventDefault();
    fetch("https://fish-be-with-you.herokuapp.com/users/register", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data !== "undefined") {
          Swal.fire({
            title: "Registration successful",
            icon: "success",
            text: "Welcome to Fish Be With You",
          });
          navigate("/login");
        } else {
          Swal.fire({
            title: "Duplicate email found!",
            icon: "error",
            text: "Please provide a different email and try again.",
          });
        }
      });
  }

  return (
    // (user.email !== null)?
    // <Navigate to='/courses' />
    // :
    <Container fluid className="pagePadding registerPage">
      <Helmet>
        <title>Register</title>
      </Helmet>
      <div className="register-container">
        <h1>Signup</h1>
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group controlId="userFirstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="userLastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="userMobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="number"
              placeholder="Mobile Number"
              value={mobileNo}
              onChange={(e) => setMobile(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password2" className="pb-2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={verifyPassword}
              onChange={(e) => SetVerifyPassword(e.target.value)}
              required
            />
          </Form.Group>
          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </Container>
  );
}
