import { Fragment, useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import AdminView from "../components/AdminView";
import UserView from "../components/UserView";

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  const fetchData = () => {
    fetch("https://fish-be-with-you.herokuapp.com/product/all")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  /*
		the map method loops through the individual course objects and returns a component for each course
		multiple components created through map method must have a unique key that will help ReactJS identify which component/elements have been changed, added or removed
	*/
  // const courses = coursesData.map(course =>{
  // 	return(
  // 		<CourseCard key={course.id} courseProp={course} />
  // 	)
  // })
  return (
    <Fragment>
      {user.isAdmin === true ? (
        <AdminView productData={products} fetchData={fetchData} />
      ) : (
        <UserView productData={products} />
      )}
    </Fragment>
  );
}
