import { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Link, Navigate, useLocation } from "react-router-dom";
// User Context
import UserContext from "../UserContext";
import logo from "../images/login.gif";

import Swal from "sweetalert2";
import { Helmet } from "react-helmet-async";

export default function Login() {
  // Allows us to consume the UserContext object and its' properties to be used for user validation
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  const retrieveUserDetails = (token) => {
    fetch("https://fish-be-with-you.herokuapp.com/users/details", {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        localStorage.setItem("name", data.firstName);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.firstName,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    fetch("https://fish-be-with-you.herokuapp.com/users/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.bearerToken !== "undefined") {
          localStorage.setItem("token", data.bearerToken);
          retrieveUserDetails(data.bearerToken);
          Swal.fire({
            title: "Login successful",
            icon: "success",
            text: "Welcome to Fish Be With You",
          });
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login credentials and try again.",
          });
        }
      });
    /*
			localStorage.setItem allows us to manipulate the browser's local storage to store information indefinitely to help demonstrate the conditional rendering
			localStorage does not trigger rerendering of components; so for it to be able to take effect, we have to refresh the browser
		*/
    // localStorage.setItem('email', email);

    /*
			using the setUser function, change the value of the user variable to the email coming from the localStorage. the argument that the setUser should get is a form of an object.

			6:11 pm
		*/
    // setUser({
    // 	email: localStorage.getItem('email')
    // });

    setEmail("");
    setPassword("");
  }

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Container id="loginContainer" className="pagePadding">
      <Helmet>
        <title>Login</title>
      </Helmet>
      <div className="login">
        {/* <Col md={6} className="homeRight">
          <img src={logo} alt="Fish be with you" className="loginGif"></img>
        </Col> */}
        <h1 className="my-3">Login</h1>
        <Form onSubmit={(e) => loginUser(e)}>
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          {isActive ? (
            <Button
              className="mb-3 w-100"
              variant="primary"
              type="submit"
              id="submitBtn"
            >
              Login
            </Button>
          ) : (
            <Button
              className="mb-3 w-100"
              variant="primary"
              type="submit"
              id="submitBtn"
              disabled
            >
              Login
            </Button>
          )}
          <div className="mb-3">
            New fish lover? <Link to={`/register`}>Create your account</Link>
          </div>
        </Form>
      </div>
    </Container>
  );
}
