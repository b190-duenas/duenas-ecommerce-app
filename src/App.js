import "./App.css";

import { UserProvider } from "./UserContext";

import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// Pages
import Home from "./pages/HomePage";
import Login from "./pages/LoginPage";
import Register from "./pages/RegisterPage";
import AppNavbar from "./components/AppNavbar";
import Logout from "./pages/LogoutPage";
import Products from "./pages/ProductPage";
import ProductView from "./components/ProductView";
import AdminView from "./components/AdminView";
import Cart from "./pages/CartPage";
import ShippingAddress from "./pages/ShippingPage";
import PaymentMethod from "./pages/PaymentPage";
import PlaceOrder from "./pages/PlaceOrderPage";
import Order from "./pages/OrderPage";
import Error from "./pages/ErrorPage";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
  useEffect(() => {
    // console.log(user);

    fetch(`http://localhost:4000/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });

          // Else set the user states to the initial values
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container fluid>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/products" element={<Products />} />
              <Route path="product/:productId" element={<ProductView />} />
              <Route path="/admin" element={<AdminView />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/payment" element={<PaymentMethod />} />
              <Route path="/shipping" element={<ShippingAddress />} />
              <Route path="/placeorder" element={<PlaceOrder />} />
              <Route path="/order/:id" element={<Order />} />
              <Route path="/*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
